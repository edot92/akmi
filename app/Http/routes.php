<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

//Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/', 'HomeController@index');

Route::get('/tampilan_navigasi_auto', 'ControllerNavigasiAuto@index');

Route::get('/tampilan_navigasi_manual', 'ControllerNavigasiManual@index');
Route::get('/manual_menu1', 'ControllerNavigasiManual@index');
Route::get('/manual_menu2', 'ControllerNavigasiManual@index');
Route::get('/manual_menu3', 'ControllerNavigasiManual@index');
