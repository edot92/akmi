@extends('adminlte::page')

@section('title', 'PROJECT AKMI SIMULATOR')

@section('content_header')
<h1>NAVIGASI KAPAL MANUAL</h1>
@stop

@section('content')
<p>SETTING KALIBRASI</p>
<!-- Centered Pills
<ul class="nav nav-pills nav-justified">
  <li  @if (Request::is('tampilan_navigasi_manual'))
	class="active"
	@endif 
  ><a href="tampilan_navigasi_manual">Home</a></li>
  <li @if (Request::is('manual_menu1'))
	class="active"
	@endif
><a href="manual_menu1">Menu 1</a></li>
  <li @if (Request::is('manual_menu2'))
	class="active"
	@endif
  ><a href="manual_menu2">Menu 2</a></li>
  <li  @if (Request::is('manual_menu3'))
	class="active"
	@endif
  ><a href="manual_menu3">Menu 3</a></li>
</ul>
-->
<ul class="nav nav-tabs nav-pills nav-justified">
  <li class="active"><a data-toggle="tab" href="#home">Home</a></li>
  <li><a data-toggle="tab" href="#menu1">Gauge Output</a></li>
  <li><a data-toggle="tab" href="#menu2">Sensor Status</a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>HOME</h3>
    <p>Some content.</p>
</div>
<div id="menu1" class="tab-pane fade">
    <!-- <h3>Gauge 1</h3> -->
    <div class="panel panel-primary">
        <div class="panel-heading">KALIBRASI POSISI GAUGE</div>
        <div class="panel-body">Panel Content
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 1</H4>
                    <div class="slider1"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 2</H4>
                    <div class="slider2"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 3</H4>
                    <div class="slider3"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 4</H4>
                    <div class="slider4"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 5</H4>
                    <div class="slider5"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 6</H4>
                    <div class="slider6"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <H4>GAUGE 7</H4>
                    <div class="slider7"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- start menu 2( sensor status) -->
<div id="menu2" class="tab-pane fade">
    <div class ="row">

     <div class="col-sm-4 col-md-3">
         <div class="panel panel-primary">
             <div class="panel-heading img-responsive center-block text-center">SENSOR GAUGE</div>
             <div class="panel-body img-responsive center-block text-center" >
                <label>Sens. Min 1 <img id ="idSensMin1" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 1 <img id ="idSensMax1" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 2 <img id ="idSensMin2" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 2 <img id ="idSensMax2" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 3 <img id ="idSensMin3" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 3 <img id ="idSensMax3" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 4 <img id ="idSensMin4" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 4 <img id ="idSensMax4" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 5 <img id ="idSensMin5" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 5 <img id ="idSensMax5" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 6 <img id ="idSensMin6" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 6 <img id ="idSensMax6" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 7 <img id ="idSensMin7" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 7 <img id ="idSensMax7" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>

                <label>Sens. Min 8 <img id ="idSensMin8" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
                <label>Sens. Max 8 <img id ="idSensMax8" src="" class="img-circle" alt="Cinque Terre" width="20" height="20" ></label>
                <p>
                    Hijau : Sensor Terkena Objek
                    <br>
                    Merah : Sensor Tidak Terkena Objek
                </p>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-md-5">
     <div class="panel panel-primary">
         <div class="panel-heading img-responsive center-block text-center">SENSOR ROTARY ENCODER</div>
         <div class="panel-body img-responsive center-block text-center" >

         </div>
     </div>
 </div>
 <!-- END KOLOM 2 -->
 <div class="col-sm-4 col-md-4">
     <div class="panel panel-primary">
         <div class="panel-heading img-responsive center-block text-center">SENSOR SPEED RPM</div>
         <div class="panel-body img-responsive center-block text-center" >
            <label>Sens. Speed 1 <img id ="idSensRpm1" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
            <label>Sens. Speed 2 <img id ="idSensRpm2" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
            <label>Sens. Speed 3 <img id ="idSensRpm3" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
            <label>Sens. Speed 4 <img id ="idSensRpm4" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>
            <label>Sens. Speed 5 <img id ="idSensRpm5" src="" class="img-circle" alt="Cinque Terre" width="20" height="20"  ></label>

            <p>
                Hijau : Sensor Terkena Objek
                <br>
                Merah : Sensor Tidak Terkena Objek
            </p>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>
<!-- END ROW 1 -->
<!-- end sensor status -->

<div class ="row">
    <div class="col-sm-12 col-md-12">
     <div class="panel panel-primary">
         <div class="panel-heading img-responsive center-block text-center">SENSOR CUACA</div>
         <div class="panel-body img-responsive center-block text-center" >
             <div class="col-md-4">
                 <h4><b>ARAH ANGIN </b></h4>
                 <canvas id="idKompasWidget"></canvas>
             </div>
             <div class="col-md-4">
                 <h4><b></b></h4>
                 <div class="col-md-8">
                     <label>Temperature:<input type="text-center" name="" value=""></label>
                     <label>     Humidity:<input type="text-center" name="" value=""></label>
                     <label>Curah Hujan<input type="text-center" name="" value=""></label>
                     <label>Tekanan Barometrik<input type="text-center" name="" value=""></label>
                     <label>Kecepatan angin(Rata2)<input type="text-center" name="" value=""></label>
                     <label>Kecepatan Angin (Maks)<input type="text-center" name="" value=""></label>
                 </div>
             </div>
             <div class="col-md-4">
                 <h4><b> </b></h4>

             </div>

         </div>
     </div>
 </div>
</div>
<!-- END ROW 2 -->
<!-- end sensor status -->

</div>
@stop

@section('adminlte_js')
<script src="{{ asset('vendor/adminlte/dist/js/app.min.js') }}"></script>
<script src="{{ asset('bower_components/canv-gauge/gauge.js') }}"></script>

@yield('js')
<!-- include the jQuery and jQuery UI scripts -->
<!-- <script src="https://code.jquery.com/jquery-2.1.1.js"></script>
-->
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

<!-- plus a jQuery UI theme, here I use "flick" -->
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.4/themes/flick/jquery-ui.css">


<!--http://simeydotme.github.io/jQuery-ui-Slider-Pips/#example-steps-stacking -->
<script src="{{ asset('bower_components/jQuery-ui-Slider-Pips/dist/jquery-ui-slider-pips.js') }}"></script>

<link rel="stylesheet" href="bower_components/jQuery-ui-Slider-Pips/dist/jquery-ui-slider-pips.css" type="text/css" />

<script>
// A $( document ).ready() block.

$( document ).ready(function() {
    console.log( "ready!" );
    var tabAktif=0;
 // or even this one if we want the earlier event
 // start cek tab yang aktif
 $("a[href='#menu1']").on('show.bs.tab', function(e) {
   tabAktif=1;
});

 $("a[href='#menu2']").on('show.bs.tab', function(e) {
   tabAktif=2;

});
 // end aktif tab cek
// start program gauge
var valGauge=[];
valGauge[0]=100;
valGauge[1]=300;
valGauge[2]=120;
valGauge[3]=320;
valGauge[4]=30;
valGauge[5]=300;
valGauge[6]=140;
valGauge[7]=300;
valGauge[8]=160;
valGauge[9]=300;
valGauge[1]=130;
valGauge[10]=180;
valGauge[11]=320;
valGauge[12]=150;
valGauge[13]=330;
valGauge[14]=300;

$(".slider1")
.slider({ 
    min: 0, 
    max: 360, 
    range: true, 
    values: [valGauge[0], valGauge[1]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[0]=ui.values[0];
    valGauge[1]=ui.values[1];
} );


$(".slider2")
.slider({ 
    min: 0, 
    max: 360, 
    range: true, 
    values: [valGauge[2], valGauge[3]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[2]=ui.values[0];
    valGauge[3]=ui.values[1];
} );
$(".slider3")
.slider({ 
    min: 0, 
    max: 360, 
    range: true, 
    values: [valGauge[4], valGauge[5]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[4]=ui.values[0];
    valGauge[5]=ui.values[1];
} );
$(".slider4")
.slider({ 
    min: 0, 
    max: 360, 
    range: true,
    values: [valGauge[6], valGauge[7]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[6]=ui.values[0];
    valGauge[7]=ui.values[1];
} );
$(".slider5")
.slider({ 
    min: 0, 
    max: 360, 
    range: true,
    values: [valGauge[8], valGauge[9]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[8]=ui.values[0];
    valGauge[9]=ui.values[1];
} );
$(".slider6")
.slider({ 
    min: 0, 
    max: 360, 
    range: true,
    values: [valGauge[10], valGauge[11]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[10]=ui.values[0];
    valGauge[11]=ui.values[1];
} );
$(".slider7")
.slider({ 
    min: 0, 
    max: 360, 
    range: true,
    values: [valGauge[12], valGauge[13]]
})
.slider("pips", {
    rest: "label"
})
.slider("float")
.on( "slidechange", function( event, ui ) {
    valGauge[12]=ui.values[0];
    valGauge[13]=ui.values[1];
} );
    //end program gauge
    //start menu 2 status sensor
    $("#idSensMin1").attr("src", "img/led_red.png");
    $("#idSensMax1").attr("src", "img/led_green.png");

    $("#idSensMin2").attr("src", "img/led_red.png");
    $("#idSensMax2").attr("src", "img/led_green.png");

    $("#idSensMin3").attr("src", "img/led_red.png");
    $("#idSensMax3").attr("src", "img/led_green.png");

    $("#idSensMin4").attr("src", "img/led_red.png");
    $("#idSensMax4").attr("src", "img/led_green.png");

    $("#idSensMin5").attr("src", "img/led_red.png");
    $("#idSensMax5").attr("src", "img/led_green.png");

    $("#idSensMin6").attr("src", "img/led_red.png");
    $("#idSensMax6").attr("src", "img/led_green.png");

    $("#idSensMin7").attr("src", "img/led_red.png");
    $("#idSensMax7").attr("src", "img/led_green.png");

    $("#idSensMin8").attr("src", "img/led_red.png");
    $("#idSensMax8").attr("src", "img/led_green.png");
    //rpm
     $("#idSensRpm1").attr("src", "img/led_green.png");
     $("#idSensRpm2").attr("src", "img/led_red.png");
     $("#idSensRpm3").attr("src", "img/led_red.png");
     $("#idSensRpm4").attr("src", "img/led_red.png");
     $("#idSensRpm5").attr("src", "img/led_red.png");
});
</script>


<script>
    window.onload = function () {

        var gauge = new Gauge({
            renderTo: 'idKompasWidget',
            width: 250,
            height: 250,
            glow: false,
            units: false,
            title: false,
            minValue: 0,
            maxValue: 360,
            majorTicks: ['S', 'SW', 'W', 'NW', 'N', 'NE', 'E', 'SE', 'S'],
            minorTicks: 22,
            ticksAngle: 360,
            startAngle: 0,
            strokeTicks: false,
            highlights: false,
            colors: {
                plate: '#222',
                majorTicks: '#f5f5f5',
                minorTicks: '#ddd',
                title: '#fff',
                units: '#ccc',
                numbers: '#eee',
                needle: {
                    start: 'rgba(240, 128, 128, 1)',
                    end: 'rgba(255, 160, 122, .9)',
                    circle: {
                        outerStart: '#ccc',
                        outerEnd: '#ccc',
                        innerStart: '#222',
                        innerEnd: '#222'
                    },
                    shadowUp: false,
                    shadowDown: false
                },
                valueBox: {
                    rectStart: '#888',
                    rectEnd: '#666',
                    background: '#babab2',
                    shadow: 'rgba(0, 0, 0, 1)'
                },
                valueText: {
                    foreground: '#444',
                    shadow: 'rgba(0, 0, 0, 0.3)'
                },
                circle: {
                    shadow: 'rgba(0, 0, 0, 0.5)',
                    outerStart: '#ddd',
                    outerEnd: '#aaa',
                    middleStart: '#eee',
                    middleEnd: '#f0f0f0',
                    innerStart: '#fff',
                    innerEnd: '#fff'
                }
            },
            circles: {
                outerVisible: false,
                middleVisible: false,
                innerVisible: true
            },
            needle: {
                type: 'line',
                end: 89,
                start: 65,
                width: 3,
                circle: {
                    size: 15,
                    inner: false,
                    outer: true
                }
            },
            valueBox: {
                visible: false
            },
            valueText: {
                visible: false
            },
            animation: {
                delay: 10,
                duration: 1000,
                fn: 'linear'
            }
        });

        gauge.onready = function () {
            setInterval(function () {
                gauge.setValue(Math.random() * (195 - 165) + 165);
            }, 1000);
        };

        gauge.setRawValue(180);

        gauge.draw();
    }
    ;
</script>

<<style type="text/css" media="screen">
#steps-fivepercent-slider .ui-slider-tip {
    visibility: visible;
    opacity: 1;
    top: -30px;
}
.nav {
    background-color: #87CEFA !important;
}

.nav>li>a {
    background-color: blue-light !important;
    color: #fff;
}
</style>
@stop